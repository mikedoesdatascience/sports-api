import datetime

import pandas as pd
from fastapi import FastAPI
from sportsml.mongo import client, load_graph_model
from sportsml.nba.data.nodes import team_abr_map as NBA_TEAMS
from sportsml.nba.data.utils import get_latest_graph
from sportsml.utils.ensemble import graph_to_df

from .routers import cbb, cfb, nba, nfl
from .utils import get_dates_odds, get_next_odds


app = FastAPI()

app.include_router(cbb.router)
app.include_router(cfb.router)
app.include_router(nba.router)
app.include_router(nfl.router)


if __name__ == "__main__":
    app.run()
