import datetime

import pandas as pd
from fastapi import APIRouter
from sportsml.mongo import client, load_graph_model
from sportsml.nba.data.nodes import team_abr_map as NBA_TEAMS
from sportsml.nba.data.utils import get_latest_graph
from sportsml.utils.ensemble import graph_to_df

from ..utils import get_dates_odds, get_next_odds


router = APIRouter(
    prefix="/nba",
    tags=["nba"],
)

nba_model = load_graph_model(
    client.nba.models.find({}).sort("date", -1).limit(1).next()
)


@router.get("/predict")
async def nba_predict():
    graph = get_latest_graph()
    preds_graph = nba_model.predict(graph)
    preds = graph_to_df(preds_graph, "home_pred", NBA_TEAMS)
    return preds.fillna(0).to_dict()


@router.get("/odds/date", summary="NBA odds by date")
async def nba_odds_date(date: str = None):
    if date is None:
        date = datetime.date.today().isoformat()
    return get_dates_odds(client.nba.odds, date)


@router.get("/odds/next", summary="Next NBA odds by date")
async def nba_odds_next(limit: int = 10):
    return get_next_odds(client.nba.odds, limit=limit)
