import datetime

from fastapi import APIRouter
from sportsml.mongo import client

from ..utils import get_dates_odds, get_next_odds


router = APIRouter(
    prefix="/nfl",
    tags=["nfl"],
)


@router.get("/odds/date", summary="NFL odds by date")
async def nfl_odds_date(date: str = None):
    if date is None:
        date = datetime.date.today().isoformat()
    return get_dates_odds(client.nfl.odds, date)


@router.get("/odds/next", summary="Next NFL odds by date")
async def nfl_odds_next(limit: int = 10):
    return get_next_odds(client.nfl.odds, limit=limit)
