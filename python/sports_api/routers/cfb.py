import datetime

from fastapi import APIRouter
from sportsml.mongo import client

from ..utils import get_dates_odds, get_next_odds


router = APIRouter(
    prefix="/cfb",
    tags=["cfb"],
)


@router.get("/odds/date", summary="CFB odds by date")
async def cfb_odds_date(date: str = None):
    if date is None:
        date = datetime.date.today().isoformat()
    return get_dates_odds(client.cfb.odds, date)


@router.get("/odds/next", summary="Next CFB odds by date")
async def cfb_odds_next(limit: int = 10):
    return get_next_odds(client.cfb.odds, limit=limit)
