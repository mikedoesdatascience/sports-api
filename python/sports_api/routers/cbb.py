import datetime

from fastapi import APIRouter
from sportsml.mongo import client

from ..utils import get_dates_odds, get_next_odds


router = APIRouter(
    prefix="/cbb",
    tags=["cbb"],
)


@router.get("/odds/date", summary="CBB odds by date")
async def cbb_odds_date(date: str = None):
    if date is None:
        date = datetime.date.today().isoformat()
    return get_dates_odds(client.cbb.odds, date)


@router.get("/odds/next", summary="Next CBB odds by date")
async def cbb_odds_next(limit: int = 10):
    return get_next_odds(client.cbb.odds, limit=limit)
