import datetime

from sportsml.mongo import client


def get_dates_odds(collection, date):
    return list(
        collection.find({"commence_time": {"$regex": f"^{date}"}}, {"_id": False})
    )


def get_next_odds(collection, limit=10):
    return list(
        collection.find(
            {"commence_time": {"$gte": f"{datetime.date.today().isoformat()}"}},
            {"_id": False},
        )
        .sort("commence_time", 1)
        .limit(limit)
    )
