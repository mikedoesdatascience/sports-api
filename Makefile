REGISTRY ?= registry.gitlab.com/mikedoesdatascience/sports_api
VERSION ?= $(shell cd python && python3 setup.py --version)

default: build

pip-lock:
	@docker build \
		--no-cache \
		-t $(REGISTRY):lock \
		-f docker/Dockerfile.lock \
		.
	@docker run -it --rm $(REGISTRY):lock > python/requirements.lock

build:
	@docker build \
		-t $(REGISTRY):$(VERSION) \
		-f docker/Dockerfile \
		.

build-prod:
	@docker build \
		-t $(REGISTRY):$(VERSION) \
		-f docker/Dockerfile.prod \
		.

push:
	@docker push $(REGISTRY):$(VERSION)

debug:
	@docker run -it --rm \
		-w /project \
		--entrypoint bash \
		$(REGISTRY):$(VERSION)

run:
	@docker run -it --rm \
		-p 8000:8000 \
		-e MONGODB_URI \
		-e MONGODB_USERNAME \
		-e MONGODB_PASSWORD \
		$(REGISTRY):$(VERSION)